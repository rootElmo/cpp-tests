# Concurrency

Here are tests related to programming concurrent/parallel programs

## Files

**Basics of concurrent programming:**

[`loose-files/threads001.cpp`](./loose-files/threads001.cpp): Basic use of a single `std::thread`

[`loose-files/threads002.cpp`](./loose-files/threads002.cpp): Basic use of a single `std::thread`, with a lambda function

[`loose-files/threads003.cpp`](./loose-files/threads003.cpp): Multiple `std::thread`s, in a vector

[`loose-files/threads004.cpp`](./loose-files/threads004.cpp): Multiple `std::jthread`s, using `C++ std 20`

[`loose-files/threads005.cpp`](./loose-files/threads005.cpp): `std::mutex` for locking shared data when using multiple threads

[`loose-files/threads006.cpp`](./loose-files/threads006.cpp): `std::lock_guard` example

[`loose-files/threads007.cpp`](./loose-files/threads007.cpp): `std::condition_variable`, notifying a working thread

[`loose-files/threads008.cpp`](./loose-files/threads007.cpp): `std::try_lock`

---