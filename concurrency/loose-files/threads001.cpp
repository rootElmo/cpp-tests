#include <iostream>
#include <thread>

// Simple test on using threads
// g++ -std=c++17 threads001.cpp -o threads001.b

void thread_pass(int x) {
    std::cout << "Hello from thread, x: " << x << std::endl;
}

int main() {
    std::thread test_thread(&thread_pass, 100);

    // Wait for thread to finish execution
    test_thread.join();
    std::cout << "Hello from main" << std::endl;
}
