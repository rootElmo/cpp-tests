# Loose files

Here are `loose files`, which don't relate to any projects, and are not lengthy enough to warrant a proper project structure. Compilation instructions are written on each file.