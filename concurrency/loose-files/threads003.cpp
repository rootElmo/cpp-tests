#include <iostream>
#include <thread>
#include <vector>

// Create multiple threads and store them in a vector
// g++ -std=c++17 threads003.cpp -o threads003.b

int main() {
    auto thread_pass=[](int x) {
       std::cout << "Hello from thread: " << std::this_thread::get_id() << std::endl;
       std::cout << "Argument in thread: " << x << std::endl;
    };
    std::vector<std::thread> threads;
    for (int i = 0; i < 10; i++) {
        threads.push_back(std::thread(thread_pass, i * 100));
    }

    for (int i = 0; i < 10; i++) {
        // Wait for thread to finish execution
        threads.at(i).join();
    }

    std::cout << "Hello from main" << std::endl;
}
