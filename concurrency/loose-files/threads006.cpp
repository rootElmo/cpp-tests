#include <iostream>
#include <thread>
#include <mutex>
#include <vector>

// Shared data for threads, std::lock_guard, deadlocks
// g++ -std=c++17 threads006.cpp -o threads006.b

// Global mutex
std::mutex g_lock;
static int shared_int = 0;

void increment_shared_value() {
    // Lock shared_int to prevent data race
    // lock_guard to prevent deadlocks
    // In case of an exception/when local_lock_guard falls out of scope
    // it is unlocked, preventing a deadlock
    std::lock_guard<std::mutex> local_lock_guard(g_lock);
    shared_int += 1;
}

int main() {

    std::vector<std::thread> threads;
    for (int i = 0; i < 10; i++) {
        threads.push_back(std::thread(increment_shared_value));
    }

    for (int i = 0; i < 10; i++) {
        // Wait for thread to finish execution
        threads.at(i).join();
    }

    std::cout << "Shared value: " << shared_int << std::endl;
}
