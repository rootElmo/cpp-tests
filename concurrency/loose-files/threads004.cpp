#include <iostream>
#include <thread>
#include <vector>

// Create multiple jthreads and store them in a vector
// g++ -std=c++20 threads004.cpp -o threads004.b

int main() {
    auto thread_pass=[](int x) {
       std::cout << "Hello from thread: " << std::this_thread::get_id() << std::endl;
       std::cout << "Argument in thread: " << x << std::endl;
    };
    std::vector<std::jthread> jthreads;
    for (int i = 0; i < 10; i++) {
        jthreads.push_back(std::jthread(thread_pass, i * 100));
    }
    std::cout << "Hello from main" << std::endl;
}
