#include <iostream>
#include <thread>
#include <mutex>
#include <vector>

// Shared data for threads, data races, std::mutex
// g++ -std=c++17 threads005.cpp -o threads005.b

// Global mutex
std::mutex g_lock;
static int shared_int = 0;

void increment_shared_value() {
    // Lock shared_int to prevent data race
    g_lock.lock();
    shared_int += 1;
    g_lock.unlock();
}

int main() {

    std::vector<std::thread> threads;
    for (int i = 0; i < 10; i++) {
        threads.push_back(std::thread(increment_shared_value));
    }

    for (int i = 0; i < 10; i++) {
        // Wait for thread to finish execution
        threads.at(i).join();
    }

    std::cout << "Shared value: " << shared_int << std::endl;
}
