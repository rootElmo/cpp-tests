#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

// std::condition_variable, working & reporting threads
// g++ -std=c++17 threads007.cpp -o threads007.b

// Global mutex
std::mutex g_lock;
std::condition_variable g_condition_variable;

int main() {

    int result = 0;
    bool notified = false;

    std::thread reporter_thread([&] {
        std::unique_lock<std::mutex> lock(g_lock);
        // Wait to be notified by working thread and try to get lock
        while(!notified) {
            g_condition_variable.wait(lock);
        }
        std::cout << "Reporter result: " << result << std::endl;
    });

    std::thread worker_thread([&] {
        // do work and notify reporter thread
        std::unique_lock<std::mutex> lock(g_lock);
        result = 30 + 15 + 2;
        notified = true;

        g_condition_variable.notify_one();
    });

    worker_thread.join();
    reporter_thread.join();
}
