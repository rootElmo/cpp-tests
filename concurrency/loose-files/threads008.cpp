#include <iostream>
#include <thread>
#include <mutex>
#include <chrono>

// std::try_lock
// g++ -std=c++17 threads008.cpp -o threads008.b

// Global mutex
std::mutex g_lock;

void job1() {
    g_lock.lock();
    std::cout << "Job 1 is executing" << std::endl;
    g_lock.unlock();
}

void job2() {
    // If thread2 cannot get lock -> wait 4 seconds
    while (!g_lock.try_lock()) {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(4s);
    }
    std::cout << "Job 2 is executing" << std::endl;
    g_lock.unlock();   
}

int main() {
    std::thread thread1(job1);
    std::thread thread2(job2);

    thread1.join();
    thread2.join();
}
