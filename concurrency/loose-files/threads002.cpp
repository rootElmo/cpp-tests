#include <iostream>
#include <thread>

// Simple test on using threads, with a lambda function
// g++ -std=c++17 threads002.cpp -o threads002.b

int main() {
    auto thread_pass=[](int x) {
       std::cout << "Hello from thread, x: " << x << std::endl;
    };

    std::thread test_thread(thread_pass, 100);

    // Wait for thread to finish execution
    test_thread.join();
    std::cout << "Hello from main" << std::endl;
}
